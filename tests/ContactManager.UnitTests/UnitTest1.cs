using System;
using FluentAssertions;
using Xunit;

namespace ContactManager.UnitTests
{
    public class UnitTest1
    {
        [Fact]
        public void TestConstructorArticleV1()
        {
            var paul = "bonjour";
            paul.Should().Be("bonjour");
        }

        [Fact]
        public void TestConstructorArticleV2()
        {
            var paul = "bonjour";
            paul.Should().Be("bonjour");
        }
    }
}
