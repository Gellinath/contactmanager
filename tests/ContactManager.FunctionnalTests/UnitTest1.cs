using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using AspNetCore.Http.Extensions;
using Avico;
using Avico.DTO;
using FluentAssertions;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using Xunit;

namespace ContactManager.FunctionnalTests
{
    public class UnitTest1
    {
        private readonly TestServer _testServer;
       private readonly HttpClient _testClient;
       public UnitTest1()
       {
           //Initializing the test environment
           _testServer = new TestServer(new WebHostBuilder()
               .UseStartup<Startup>());
           //Test client creation
           _testClient = _testServer.CreateClient();
       }

       //---------------Get Contact-----------------------------//

       [Fact]
       public async Task TestGetAllContactRequestAsync()
       {
           var response = await _testClient.GetAsync("/api/contacts");
           response.EnsureSuccessStatusCode();
       }

       [Fact]
       public async Task TestGetContactByIdRequestOkAsync()
       {
           var response = await _testClient.GetAsync("/api/contacts/1");
           response.StatusCode.Should().Be(HttpStatusCode.OK);
       }

       [Fact]
       public async Task TestGetByIdRequestNotFoundAsync()
       {
           var response = await _testClient.GetAsync("/api/contacts/"+ int.MaxValue);
           response.StatusCode.Should().Be(HttpStatusCode.NotFound);
       }

//--------------------------------Put contact--------------------------//
       [ Fact]
        public async Task TestPutRequestAndResponseOkAsync()
        {
            var contactId = 1;
            var contact = new Contacts
            {
                Id = contactId ,
                Prenom = "Mich'L" ,
                Nom = "DU 56",
                Adresses = null,
                Telephones = null
            };

            using var responsePut = await
            _testClient.PutAsJsonAsync ("/api/contacts/" + contactId, contact );

            // Assert
            responsePut.StatusCode.Should().Be(HttpStatusCode.OK);
        }

        [ Fact]
        public async Task TestPutRequestAndResponseBadRequestV1Async()
        {
            var contactId = 1;
            var contact = new Contacts
            {
                Id = contactId ,
                Prenom = "Mich'L" ,
                Nom = "DU 56",
                Adresses = null,
                Telephones = null,
            };

            using var responsePut = await
            _testClient.PutAsJsonAsync ("/api/contacts/" + int.MaxValue, contact );

            // Assert
            responsePut.StatusCode.Should().Be(HttpStatusCode.BadRequest);
        }

        [ Fact]
        public async Task TestPutRequestAndResponseBadRequestV2Async()
        {
            var contactId = 1;
            var contact = new Contacts
            {
                Id = int.MaxValue ,
                Prenom = "Mich'L" ,
                Nom = "DU 56",
                Adresses = null,
                Telephones = null,
            };

            using var responsePut = await
            _testClient.PutAsJsonAsync ("/api/contacts/" + contactId, contact );

            // Assert
            responsePut.StatusCode.Should().Be(HttpStatusCode.BadRequest);
        }

        [ Fact]
        public async Task TestPutRequestAndResponseNotFoundAsync()
        {
            var contactId = int.MaxValue;
            var contact = new Contacts
            {
                Id = int.MaxValue ,
                Prenom = "Mich'L" ,
                Nom = "DU 56",
                Adresses = null,
                Telephones = null
            };

            using var responsePut = await 
            _testClient.PutAsJsonAsync ("/api/contacts/" + contactId, contact );

            // Assert
            responsePut.StatusCode.Should().Be(HttpStatusCode.NotFound);
        }

//--------------------------------Delete Contact---------------------------//

        [ Fact]
        public async Task TestDeleteRequestNoContentAsync ()
        {
            var contactId = 2;
            using var responseDelete = await 
            _testClient.DeleteAsync ("/api/contacts/" + contactId );

            // Assert
            responseDelete.StatusCode.Should().Be(HttpStatusCode.NoContent);
        }

        [ Fact]
        public async Task TestDeleteRequestNotFoundAsync ()
        {
            var contactId = Int16.MaxValue;
            using var responseDelete = await 
            _testClient.DeleteAsync ("/api/contacts/" + contactId );

            // Assert
            responseDelete.StatusCode.Should().Be(HttpStatusCode.NotFound);
        }

//-----------------------Get Adresses-----------------------//

        [Fact]
       public async Task TestGetAllAdresseByContactIdRequestOkAsync()
       {
           var response = await _testClient.GetAsync("/api/contacts/1/adresses");
           response.StatusCode.Should().Be(HttpStatusCode.OK);
       }

        [Fact]
       public async Task TestGetAllAdresseByContactIdRequestNotFoundAsync()
       {
           var response = await _testClient.GetAsync("/api/contacts/2/adresses");
           response.StatusCode.Should().Be(HttpStatusCode.NotFound);
       }

//-----------------------Get Telephones-----------------------//

        [Fact]
       public async Task TestGetAllTelephonesByContactIdRequestOkAsync()
       {
           var response = await _testClient.GetAsync("/api/contacts/1/telephones");
           response.StatusCode.Should().Be(HttpStatusCode.OK);
       }

        [Fact]
       public async Task TestGetAllTelephonesByContactIdRequestNotFoundAsync()
       {
           var response = await _testClient.GetAsync("/api/contacts/2/telephones");
           response.StatusCode.Should().Be(HttpStatusCode.NotFound);
       }

//--------------------------------Put adresse--------------------------//
       [ Fact]
        public async Task TestPutAdresseRequestAndResponseOkAsync()
        {
            var adresseId = 1;
            var adresse = new Adresse
            {
                Id = adresseId ,
                NumeroRue = 1,
                NomRue = "Perdu",
                NomVille = "Angoulême",
                CodePostal = 16000
            };

            using var responsePut = await
            _testClient.PutAsJsonAsync ("/api/contacts/1/adresses/" + adresseId, adresse );

            // Assert
            responsePut.StatusCode.Should().Be(HttpStatusCode.OK);
        }

        [ Fact]
        public async Task TestPutAdresseRequestAndResponseNotFoundAsync()
        {
            var adresseId = int.MaxValue;
            var adresse = new Adresse
            {
                Id = adresseId ,
                NumeroRue = 1,
                NomRue = "Perdu",
                NomVille = "Angoulême",
                CodePostal = 16000
            };

            using var responsePut = await
            _testClient.PutAsJsonAsync ("/api/contacts/1/adresses/" + adresseId, adresse );

            // Assert
            responsePut.StatusCode.Should().Be(HttpStatusCode.NotFound);
        }

        [ Fact]
        public async Task TestPutAdresseRequestAndResponseNotFoundContactAsync()
        {
            var adresseId = 1;
            var adresse = new Adresse
            {
                Id = adresseId ,
                NumeroRue = 1,
                NomRue = "Perdu",
                NomVille = "Angoulême",
                CodePostal = 16000
            };

            using var responsePut = await
            _testClient.PutAsJsonAsync ("/api/contacts/"+int.MaxValue+"/adresses/" + adresseId, adresse );

            // Assert
            responsePut.StatusCode.Should().Be(HttpStatusCode.NotFound);
        }

        [ Fact]
        public async Task TestPutAdresseRequestAndResponseBadRequestAsync()
        {
            var adresseId = 1;
            var adresse = new Adresse
            {
                Id = int.MaxValue ,
                NumeroRue = 1,
                NomRue = "Perdu",
                NomVille = "Angoulême",
                CodePostal = 16000
            };

            using var responsePut = await
            _testClient.PutAsJsonAsync ("/api/contacts/1/adresses/" + adresseId, adresse );

            // Assert
            responsePut.StatusCode.Should().Be(HttpStatusCode.BadRequest);
        }

        [ Fact]
        public async Task TestPutAdresseRequestAndResponseBadRequestV2Async()
        {
            var adresseId = 1;
            var adresse = new Adresse
            {
                Id = adresseId ,
                NumeroRue = 1,
                NomRue = "Perdu",
                NomVille = "Angoulême",
                CodePostal = 16000
            };

            using var responsePut = await
            _testClient.PutAsJsonAsync ("/api/contacts/1/adresses/" + int.MaxValue, adresse );

            // Assert
            responsePut.StatusCode.Should().Be(HttpStatusCode.BadRequest);
        }

//--------------------------------Put telephone--------------------------//
       [ Fact]
        public async Task TestPutTelephoneRequestAndResponseOkAsync()
        {
            var telephoneId = 1;
            var telephone = new Telephone
            {
                Id = telephoneId ,
                NumeroTelephone = 0606060606
            };

            using var responsePut = await
            _testClient.PutAsJsonAsync ("/api/contacts/1/telephones/" + telephoneId, telephone );

            // Assert
            responsePut.StatusCode.Should().Be(HttpStatusCode.OK);
        }

        [ Fact]
        public async Task TestPutTelephoneRequestAndResponseNotFoundAsync()
        {
            var telephoneId = int.MaxValue;
            var telephone = new Telephone
            {
                Id = telephoneId ,
                NumeroTelephone = 0606060606
            };

            using var responsePut = await
            _testClient.PutAsJsonAsync ("/api/contacts/1/telephones/" + telephoneId, telephone );

            // Assert
            responsePut.StatusCode.Should().Be(HttpStatusCode.NotFound);
        }

        [ Fact]
        public async Task TestPutTelephoneRequestAndResponseNotFoundContactAsync()
        {
            var telephoneId = 1;
            var telephone = new Telephone
            {
                Id = telephoneId ,
                NumeroTelephone = 0606060606
            };

            using var responsePut = await
            _testClient.PutAsJsonAsync ("/api/contacts/"+int.MaxValue+"/telephones/" + telephoneId, telephone );

            // Assert
            responsePut.StatusCode.Should().Be(HttpStatusCode.NotFound);
        }

        [ Fact]
        public async Task TestPutTelephoneRequestAndResponseBadRequestAsync()
        {
            var telephoneId = 1;
            var telephone = new Telephone
            {
                Id = int.MaxValue ,
                NumeroTelephone = 0606060606
            };

            using var responsePut = await
            _testClient.PutAsJsonAsync ("/api/contacts/1/telephones/" + telephoneId, telephone );

            // Assert
            responsePut.StatusCode.Should().Be(HttpStatusCode.BadRequest);
        }

        [ Fact]
        public async Task TestPutTelephoneRequestAndResponseBadRequestV2Async()
        {
            var telephoneId = 1;
            var telephone = new Telephone
            {
                Id = telephoneId ,
                NumeroTelephone = 0606060606
            };

            using var responsePut = await
            _testClient.PutAsJsonAsync ("/api/contacts/1/telephones/" + int.MaxValue, telephone );

            // Assert
            responsePut.StatusCode.Should().Be(HttpStatusCode.BadRequest);
        }

//--------------------------------Delete adresse---------------------------//

        [ Fact]
        public async Task TestDeleteAdresseRequestNoContentAsync ()
        {
            var adresseId = 2;
            using var responseDelete = await 
            _testClient.DeleteAsync ("/api/contacts/1/adresses/" + adresseId );

            // Assert
            responseDelete.StatusCode.Should().Be(HttpStatusCode.NoContent);
        }

        [ Fact]
        public async Task TestDeleteAdresseRequestNotFoundAsync ()
        {
            var adresseId = int.MaxValue;
            using var responseDelete = await 
            _testClient.DeleteAsync ("/api/contacts/1/adresses/" + adresseId );

            // Assert
            responseDelete.StatusCode.Should().Be(HttpStatusCode.NotFound);
        }

        [ Fact]
        public async Task TestDeleteAdresseRequestNotFoundContactAsync ()
        {
            var adresseId = 1;
            using var responseDelete = await 
            _testClient.DeleteAsync ("/api/contacts/"+int.MaxValue+"/adresses/" + adresseId );

            // Assert
            responseDelete.StatusCode.Should().Be(HttpStatusCode.NotFound);
        }

//--------------------------------Delete telephone---------------------------//

        [ Fact]
        public async Task TestDeleteTelephoneRequestNoContentAsync ()
        {
            var telephoneId = 1;
            using var responseDelete = await 
            _testClient.DeleteAsync ("/api/contacts/1/telephone/" + telephoneId );

            // Assert
            responseDelete.StatusCode.Should().Be(HttpStatusCode.NoContent);
        }

        [ Fact]
        public async Task TestDeleteTelephoneRequestNotFoundAsync ()
        {
            var telephoneId = int.MaxValue;
            using var responseDelete = await 
            _testClient.DeleteAsync ("/api/contacts/1/telephone/" + telephoneId );

            // Assert
            responseDelete.StatusCode.Should().Be(HttpStatusCode.NotFound);
        }

        [ Fact]
        public async Task TestDeleteTelephoneRequestNotFoundContactAsync ()
        {
            var telephoneId = 1;
            using var responseDelete = await 
            _testClient.DeleteAsync ("/api/contacts/"+int.MaxValue+"/telephone/" + telephoneId );

            // Assert
            responseDelete.StatusCode.Should().Be(HttpStatusCode.NotFound);
        }

    }
}
